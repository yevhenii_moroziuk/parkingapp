﻿using System;
using System.Collections.Generic;
using ParkingApp.Helpers;
using ParkingLibrary;

namespace ParkingApp
{
	public class MainProcess
	{
		public static void SomeAction()
		{
			Parking _parking = Parking.Initialize(new ParkingConsole());

			Helper.ShowInfo();
			int input = Helper.InputInteger(1,10);

			switch (input)
			{
				case 1:
					Console.Write("Income for last minute: ");
					Console.WriteLine(_parking.ShowIncomeForLastMinute());
					Helper.InMenuMessage();
					break;
				case 2:
					Console.WriteLine($"Count of parking places:" +
						$" Total: {Setting.MaxCountOfParkingPlaces} Free: {_parking.ShowCountOfFreePlaces()}");
					Helper.InMenuMessage();
					break;
				case 3:
					Console.WriteLine("All transactions for last minute:");
					_parking.ShowAllTransactionsForLastMinute();
					Helper.InMenuMessage();
					break;
				case 4:
					Console.WriteLine("Full transactions history: ");
					_parking.ShowFullTransactionHistory();
					Helper.InMenuMessage();
					break;
				case 5:
					Console.WriteLine("All vehicles:");
					_parking.ShowAllVehicles();
					Helper.InMenuMessage();
					break;
				case 6:
					Helper.PrintColoredMessage("Add car:", ConsoleColor.Green);
					Console.WriteLine("Input 1 - Car, 2 - Bus, 3 - Truck, 4 - Motorcycle. Press -1 to leave");

					int carType = Helper.InputInteger(1,4);
					if (carType == -1)
					{
						Console.Clear();
						break;
					}
					
					Console.WriteLine("Input car balance");
					decimal balance = Helper.InputDecimal();

					_parking.AddCar((VehicleType)carType, balance);					
					Helper.InMenuMessage();
					break;
				case 7:
					Helper.PrintColoredMessage("Delete vehicle:", ConsoleColor.Red);
					Console.WriteLine("Input car id. Press -1 to leave");

					int id = Helper.InputInteger();
					if (id == -1)
					{
						Console.Clear();
						break;
					}
					_parking.RemoveCar(id);
					Helper.InMenuMessage();
					break;
				case 8:
					Helper.PrintColoredMessage("Top up the balance on specific vehicle:", ConsoleColor.Blue);
					Console.WriteLine("Input car id. Press -1 to leave");

					int carid = Helper.InputInteger();
					if (carid == -1)
					{
						Console.Clear();
						break;
					}
					Console.WriteLine("Input car balance");
					decimal money = Helper.InputDecimal();

					_parking.TopUpCarBalance(carid, money);
					Helper.InMenuMessage();
					break;
				case 9:
					Console.Write("Parking balance: ");
					Console.WriteLine(_parking.ShowBalance());
					Helper.InMenuMessage();
					break;
				case 10:
					Helper.Close();
					break;
			}
		}
	}
}
