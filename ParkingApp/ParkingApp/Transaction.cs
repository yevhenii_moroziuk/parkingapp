﻿using System;

namespace ParkingApp
{
	public class Transaction
	{
		private static int _countOfTransactions = 0;

		private int _transactionId;

		public decimal TransactionSum { get; set; }

		private DateTime _transactionDate;

		public Transaction(decimal sum)
		{
			_transactionId = _countOfTransactions;
			TransactionSum = sum;
			_transactionDate = DateTime.Now;
			_countOfTransactions++;
		}
		public override string ToString()
		{
			return $"Transaction id: {_transactionId},Transaction date: {_transactionDate.ToLocalTime()}, Transaction sum: {TransactionSum}";
		}

	}
}
