﻿using ParkingApp;
using System.Threading.Tasks;

namespace ParkingLibrary.Parkinglibrary
{
	public interface IParking
	{
		/*
		Сумму заработанных денег за последнюю минуту.
		Узнать количество свободных/занятых мест на парковке.
		Вывести на экран все Транзакции Парковки за последнюю минуту
		Вывести всю историю Транзакций (считав данные из файла Transactions.log)
		Вывести на экран список всех Транспортных средств
		Создать/поставить Транспортное средство на Парковку 
		Удалить/забрать Транспортное средство с Парковки 
		Пополнить баланс конкретного Транспортного средства 
		 */

		decimal GetIncomeForOneMinute();
		int GetCountOfFreePlaces();
		void ShowAllTransactionsForLastMinute();
		void ShowAllTransactions();
		void ShowAllCars();
		void AddCar(VehicleType type, decimal balance);
		void DeleteCar(int id);
		void TopUpCarBalance(int id, decimal money);
		Task WriteTransactionsToLog();
		decimal GetBalance();
		void CreateTransaction();
	}
}
