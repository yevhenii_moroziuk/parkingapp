﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ParkingApp;
using ParkingApp.Parkinglibrary;
using ParkingLibrary.Parkinglibrary;

namespace ParkingLibrary
{
	public class ParkingConsole : IParking
	{
		private List<Car> _cars;
		private List<Transaction> _transactions;//transaction for 1 last minute
		private decimal _balance;
		private LoggingFile _logging;

		public ParkingConsole()
		{
			_cars = new List<Car>();
			_transactions = new List<Transaction>();
			_balance = Setting.DefaultBalance;
			_logging = new LoggingFile();
		}
		public void AddCar(VehicleType type, decimal balance)
		{
			if (_cars.Count < Setting.MaxCountOfParkingPlaces)
			{
				Car car = new Car(type, balance);
				_cars.Add(car);
			}
			else
			{
				Console.WriteLine("No free places in the parking");
			}
		}

		public void CreateTransaction()
		{
			decimal transactionSum = 0;

			foreach (Car car in _cars)
			{
				transactionSum += Pay(car);
			}

			_transactions.Add(new Transaction(sum: transactionSum));

			_balance += transactionSum;
		}

		public void DeleteCar(int id)
		{
			var targetCar = _cars.SingleOrDefault(c => c.CarId == id);

			if (targetCar != null)
				_cars.Remove(targetCar);
			else
				Console.WriteLine("this car isn`t exist");
		}

		public int GetCountOfFreePlaces()
		{
			return Setting.MaxCountOfParkingPlaces - _cars.Count();
		}

		public decimal GetIncomeForOneMinute()
		{
			decimal income = 0;
			foreach (Transaction transaction in _transactions)
			{
				income += transaction.TransactionSum;
			}
			return income;
		}

		public void ShowAllCars()
		{
			foreach (Car car in _cars)
			{
				Console.WriteLine(car.ToString());
			}
		}

		public void ShowAllTransactions()
		{
			_logging.ReadTransactionLog();
		}

		public void ShowAllTransactionsForLastMinute()
		{
			foreach (Transaction transaction in _transactions)
			{
				Console.WriteLine(transaction.ToString());
			}
		}

		public void TopUpCarBalance(int id, decimal money)
		{
			var targetCar = _cars.SingleOrDefault(c => c.CarId == id);

			if (targetCar != null)
				targetCar.CurrentBalance += money;
			else
				Console.WriteLine("this car isn`t exist");
		}

		private decimal Pay(Car car)
		{
			decimal payment = 0;

			if (car.CurrentBalance - Setting.Tarif[car.Type.ToString()] <= 0)
			{
				payment += Setting.PenaltyCoeff * Setting.Tarif[car.Type.ToString()];
			}
			else
			{
				payment += Setting.Tarif[car.Type.ToString()];
			}

			car.CurrentBalance -= payment;

			return payment;
		}

		public async Task WriteTransactionsToLog()
		{
			await _logging.WriteTransactionLog($"Date: {DateTime.Now.ToLocalTime()}, income: {GetIncomeForOneMinute()}, total sum: {_balance}");
			_transactions.Clear();

		}

		public decimal GetBalance()
		{
			return _balance;
		}
	}
}
