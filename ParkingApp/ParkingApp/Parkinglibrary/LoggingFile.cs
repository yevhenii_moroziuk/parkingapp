﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace ParkingApp.Parkinglibrary
{
	public class LoggingFile
	{
		private string _filePath = Setting.FilePath;
		public void ReadTransactionLog()
		{

			try
			{
				using (StreamReader r = File.OpenText(_filePath))
				{
					string line;
					while ((line = r.ReadLine()) != null)
					{
						Console.WriteLine(line);
					}
				};
				
			}
			catch(Exception e)
			{
				Console.WriteLine(e.Message);
			}
		}

		public async Task WriteTransactionLog(string text)
		{
			try
			{
				using (var w = new FileStream(_filePath, FileMode.OpenOrCreate))
				{
					w.Seek(0, SeekOrigin.End);
					await w.WriteAsync(Encoding.Default.GetBytes(text + Environment.NewLine));
					await w.WriteAsync(Encoding.Default.GetBytes(Setting.LogSepartor));
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
			}
		}
	}
}
