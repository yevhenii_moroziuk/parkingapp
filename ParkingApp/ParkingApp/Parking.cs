﻿using ParkingApp.Parkinglibrary;
using ParkingLibrary.Parkinglibrary;
using System;
using System.Threading;

namespace ParkingApp
{
	public class Parking
	{
		private static Parking _parking = null;
		private IParking _plibrary;
		private Parking(IParking library)
		{
			_plibrary = library;

			Thread thread = new Thread(()=> 
			{
				int resetCounter = 0;

				while (true)
				{
					Thread.Sleep(Setting.TransactionInterval);
					if (_parking.ShowCountOfFreePlaces() < Setting.MaxCountOfParkingPlaces)
					{
						resetCounter++;
						CreateTransaction();
						if(resetCounter > 11)
						{
							resetCounter = 0;
							WriteLogAsync();
						}
					}
					
				}
			});
			thread.Start();			
		}

		public static Parking Initialize(IParking library)
		{
			if (_parking == null)
				_parking = new Parking(library);

			return _parking;
		}

		public void ShowAllVehicles()
		{
			_plibrary.ShowAllCars();
		}

		public void AddCar(VehicleType type, decimal balance)
		{
			_plibrary.AddCar(type, balance);
		}

		public void RemoveCar(int id)
		{
			_plibrary.DeleteCar(id);
		}

		public void TopUpCarBalance(int id, decimal money)
		{
			_plibrary.TopUpCarBalance(id, money);
		}

		public void ShowFullTransactionHistory()
		{
			_plibrary.ShowAllTransactions();
		}

		public int ShowCountOfFreePlaces()
		{
			return _plibrary.GetCountOfFreePlaces();
		}

		public decimal ShowIncomeForLastMinute()
		{
			return  _plibrary.GetIncomeForOneMinute();
		}

		public void ShowAllTransactionsForLastMinute()
		{
			_plibrary.ShowAllTransactionsForLastMinute();
		}

		public decimal ShowBalance()
		{
			return _plibrary.GetBalance();
		}

		public void WriteLogAsync()
		{
			_plibrary.WriteTransactionsToLog();
		}

		public void CreateTransaction()
		{
			_plibrary.CreateTransaction();
		}

	}
}
